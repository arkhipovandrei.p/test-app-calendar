import s from "./styles.module.scss"
import {FC, useMemo, useState} from "react";
import dayjs, {Dayjs} from "dayjs";
import {dayjsRange} from "../../../app/utils/date.ts";
import {IconMoneyWallet} from "../icons/icon-money-wallet.tsx";
import cn from "classnames";
import Button from "../button/Button.tsx";
import {IconQuestion} from "../icons/icon-question.tsx";
import {IconPrev} from "../icons/icon-prev.tsx";
import {IconNext} from "../icons/icon-next.tsx";


type Event = {
  id: number;
  name: string
}

type CalendarProps = {
  events: Array<Event>;
  onChange(date: Dayjs): Promise<void>
}

const Calendar: FC<CalendarProps> = (props) => {

  const {events, onChange} = props;

  const [currentDate, setCurrentDate] = useState<Dayjs>(dayjs());

  const changeCurrentDate = (date: Dayjs) => {
    setCurrentDate(date);
    return onChange(date);
  }

  const onPrev = () => changeCurrentDate(dayjs(currentDate).subtract(1, 'month'));
  const onNext = () => changeCurrentDate(dayjs(currentDate).add(1, 'month'));

  const from = dayjs(currentDate).startOf('month').day(0);
  const to = dayjs(currentDate).endOf('month').day(6)

  const dates = useMemo(() => {
    return dayjsRange(from, to)
  }, [from, to]);

  return <div className={s.calendar}>
    <div className={s.calendarToolbar}>
      <div className={s.calendarToolbarCurrentDate}>
        <div className={s.calendarToolbarCurrentDateButton} onClick={onPrev}>
          <IconPrev/>
        </div>
        <span className={s.calendarToolbarCurrentDateLabel}>
          {currentDate.format('MMM YYYY')}
        </span>
        <div className={s.calendarToolbarCurrentDateButton} onClick={onNext}>
          <IconNext/>
        </div>
      </div>

      <Button
        variant={"bordered"}
        color={"primary"}>
        Сегодня
      </Button>

      <div className="text-dark-violet">
        <IconQuestion/>
      </div>
    </div>
    <CalendarWeeks dates={dates}/>
    <div className={s.calendarDates}>
      {
        dates.map((day) => {
          const date = dayjs(day);
          return <CalendarDate
            date={date}
            currentDate={currentDate}
            key={date.unix()}
          />
        })
      }
    </div>
  </div>
}

export default Calendar;

const CalendarDate: FC<{ date: Dayjs; currentDate: Dayjs }> = ({date, currentDate}) => {

  const isInvalid = useMemo(() => {

    return !dayjs(date).isSame(currentDate, 'month')

  }, [date])

  return <div className={cn(s.calendarDate, {
    [s.calendarDateDisabled]: isInvalid
  })}>
    <div className={s.calendarDateDay}>
      {date.format('D')}
    </div>
    <div className={s.calendarDateEvents}>
      <CalendarEvent/>
      <CalendarEvent/>
    </div>
  </div>
};

const CalendarEvent = () => {
  return <div className={s.calendarEvent}>
    <div className={s.calendarEventTime}>
      <time>13:00-13:45</time>
      <div className={s.calendarEventIcon}>
        <IconMoneyWallet size={12}/>
      </div>
    </div>
    <div className={s.calendarEventLesson}>
      Ментальная арифметика
    </div>
  </div>
}

const CalendarWeeks:FC<{dates:Array<Dayjs | string>}> = ({dates}) => {
  return <div className={s.calendarWeeks}>
    {
      dates.slice(0, 7).map(date => {
        return <div className={s.calendarWeekName}>
          {dayjs(date).toDate().toLocaleDateString('en', { weekday:"short"})}
        </div>
      })
    }
  </div>;
}
