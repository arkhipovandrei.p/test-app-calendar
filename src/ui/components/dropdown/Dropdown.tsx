import {type FC} from "react";
import s from './styles.module.scss'
import cn from "classnames";
import {DropdownProps} from "./types/DropdownProps.ts";
import DropdownToggle from "./DropdownToggle.tsx";
import DropdownContent from "./DropdownContent.tsx";
import {useDropDown} from "./useCases/useDropDown.ts";


const Dropdown: FC<DropdownProps> = (props) => {

  const {
    showArrow,
    children,
    contentProps
  } = props;
  const {

    isOpen,
    isVisibleContent,
    ref,
    handleToggle
  } = useDropDown(props)

  return <div
    className={cn(s.dropdown,  {
      [s.dropdownOpen]: isOpen
    })}
    ref={ref}>
    <DropdownToggle onToggle={handleToggle} showArrow={showArrow}>
      {children}
    </DropdownToggle>
    {isVisibleContent && <DropdownContent {...contentProps} onClose={handleToggle}/>}
  </div>
}

export default Dropdown;
