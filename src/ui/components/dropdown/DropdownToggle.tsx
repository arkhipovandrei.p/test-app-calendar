import type {FC} from "react";
import s from './styles.module.scss'
import {IconArrow} from "../icons/icon-arrow.tsx";
import {DropdownToggleProps} from "./types/DropdownToggleProps.ts";


const DropdownToggle:FC<DropdownToggleProps> = (props) => {
  const {
    showArrow,
    children,
    onToggle
  } = props;

  return <>
    <div className={s.dropdownToggle} onClick={onToggle}>
      {children}
      {showArrow && <div className={s.dropdownArrow}>
        <IconArrow/>
      </div>}
    </div>
  </>
}

export default DropdownToggle
