import {ReactNode} from "react";

export type DropdownToggleProps = {
  showArrow?: boolean;
  children: ReactNode;
  onToggle():void
}
