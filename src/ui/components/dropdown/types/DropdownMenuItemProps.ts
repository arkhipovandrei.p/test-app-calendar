import type {Key, ReactNode} from "react";

export type DropdownMenuItemProps = {
  key: Key,
  label: ReactNode;
  isActive?: boolean;
}
