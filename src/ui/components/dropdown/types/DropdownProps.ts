import {ReactNode} from "react";
import {DropdownContentProps} from "./DropdownContentProps.ts";

export type DropdownProps = {
  children: ReactNode;
  showArrow?: boolean;
  contentProps?: DropdownContentProps
}
