import {type ReactNode} from "react";
import {DropdownMenuItemProps} from "./DropdownMenuItemProps.ts";

export type DropdownContentProps = {
  onClose?: () => void;
  header?: ReactNode;
  footer?: ReactNode;
  items?: Array<DropdownMenuItemProps> | null
}
