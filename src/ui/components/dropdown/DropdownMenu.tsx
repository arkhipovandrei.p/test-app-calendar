import {type FC} from "react";
import s from './styles.module.scss'
import {DropdownMenuItemProps} from "./types/DropdownMenuItemProps.ts";
import DropdownMenuItem from "./DropdownMenuItem.tsx";

const DropdownMenu: FC<{ items?: Array<DropdownMenuItemProps> }> = ({items}) => {

  if (!items?.length) return null

  return <>
    <ul className={s.dropdownMenu}>
      {items.map(item => <DropdownMenuItem
        key={item.key}
        item={item}
      />)}
    </ul>
  </>
}

export default DropdownMenu
