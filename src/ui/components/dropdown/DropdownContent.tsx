import {type FC} from "react";
import s from './styles.module.scss'
import {IconClose} from "../icons/icon-close.tsx";
import DropdownMenu from "./DropdownMenu.tsx";
import Divider from "../divider/Divider.tsx";
import {DropdownContentProps} from "./types/DropdownContentProps.ts";

const DropdownContent: FC<DropdownContentProps> = (props) => {
  const {
    onClose,
    header,
    footer,
    items
  } = props;

  return <>
    <div className={s.dropdownContent}>

      {onClose && <div className={s.dropdownContentClosed} onClick={onClose}>
        <IconClose/>
      </div>}

      <div className={s.dropdownContentHeader}>
        {header}
      </div>

      {items && <DropdownMenu items={items}/>}

      {footer && <Divider/>}

      <div className={s.dropdownContentFooter}>
        {footer}
      </div>

    </div>
  </>
}

export default DropdownContent;
