import {useMemo, useRef, useState} from "react";
import {useOnClickOutside} from "usehooks-ts";
import {DropdownProps} from "../types/DropdownProps.ts";

export const useDropDown = (props: DropdownProps) => {
  const {
    contentProps
  } = props;

  const [isOpen, setIsOpen] = useState(false);
  const ref = useRef(null)

  const handleClose = () => {
    setIsOpen(false);
  }
  const handleToggle = () => {
    setIsOpen(prev => !prev);
  }

  useOnClickOutside(ref, handleClose);

  const isVisibleContent = useMemo(() => {
    return isOpen && contentProps
  }, [isOpen, contentProps]);

  return {
    isOpen,
    isVisibleContent,
    ref,
    handleToggle
  }

}
