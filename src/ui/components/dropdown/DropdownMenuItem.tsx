import type {FC} from "react";
import {DropdownMenuItemProps} from "./types/DropdownMenuItemProps.ts";
import cn from "classnames";
import s from "./styles.module.scss";

const DropdownMenuItem: FC<{ item: DropdownMenuItemProps }> = ({item}) => {
  return <>
    <li
      className={cn(s.dropdownMenuItem, {
        [s.dropdownMenuItemActive]: item.isActive
      })}>
      {item.label}
    </li>
  </>
}
export default DropdownMenuItem;
