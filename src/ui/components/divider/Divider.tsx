import s from './styles.module.scss'

const Divider = () => {
  return <hr className={s.divider}/>
}

export default Divider;
