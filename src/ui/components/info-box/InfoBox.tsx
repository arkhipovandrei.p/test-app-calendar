import s from './styles.module.scss';
import type {FC, ReactNode} from "react";
import cn from 'classnames'

type InfoBoxProps =  {
  children?: ReactNode;
  color?: 'blue' | 'violet'
  header: ReactNode;
  icon?: ReactNode;
}

const InfoBox:FC<InfoBoxProps> = (props) => {

  const {
    children,
    header,
    icon,
    color
  } = props

  return <>
    <div className={cn(s.card , {
      [s[`card--${color}`]]: color
    } )}>

      <div className={s.cardHeader}>
        {header}
      </div>

      <div className={s.cardBody}>
        <div>{children}</div>
        {icon && <div className={s.cardIcon}>
          {icon}
        </div>}
      </div>

    </div>
  </>
}

export default InfoBox;
