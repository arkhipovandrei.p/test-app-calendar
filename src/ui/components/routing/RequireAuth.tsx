import {useSelector} from "react-redux";
import {Navigate, Outlet, useLocation} from "react-router-dom";
import BaseLayout from "../../layouts/base-layout/BaseLayout.tsx";
import {type FC} from "react";

const RequireAuth:FC = () => {
  const { userToken} = useSelector((state) => state.auth);
  const location = useLocation();

  if (!userToken) {
    return <Navigate to="/login" state={{from: location}} replace/>;
  }
  return <BaseLayout><Outlet/></BaseLayout>;
}

export default RequireAuth
