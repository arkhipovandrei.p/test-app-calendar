import type {ButtonHTMLAttributes, ElementType, FC, ReactNode} from "react";
import s from './styles.module.scss'
import cn from "classnames";
import {LinkProps} from "react-router-dom";


const ButtonSize = {
  xS: "xs",
  sm: "sm",
  md: "md",
  lg: "lg",
  xl: "xl",
  xxl: "xxl"
}

const ButtonColor = {
  ghost: "ghost",
  danger: "danger",
  primary: "primary",
  info: "info",
  secondary: "secondary",
  default: "default",
} as const;

const ButtonVariant = {
  solid: "solid",
  dashed: "dashed",
  bordered: "bordered",
} as const;

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & Partial<LinkProps> & {
  children?: ReactNode;
  size?: typeof ButtonSize[keyof typeof ButtonSize]
  color?: typeof ButtonColor[keyof typeof ButtonColor];
  variant?: typeof ButtonVariant[keyof typeof ButtonVariant];
  as?: ElementType
  fullWidth?: boolean;
}

const useButton = (props: ButtonProps) => {
  const {
    as,
    children,
    size = ButtonSize.md,
    color = ButtonColor.primary,
    variant = ButtonVariant.solid,
    fullWidth,
    className,
    ...attributes
  } = props;

  const Component = as || "button";

  const classNames = cn(s.button, className,{
    [s[`button--${variant}`]]: variant,
    [s[`button--${color}`]]: color,
    [s[`button--${size}`]]: size,
    [s.buttonFull]: fullWidth,
  })

  return {
    Component,
    children,
    attributes: {
      ...attributes,
      className: classNames,
    },
  }
}


const Button: FC<ButtonProps> = (props) => {
  const {
    Component,
    children,
    attributes,
  } = useButton(props)

  return <Component {...attributes}>
    {children}
  </Component>
}
export default Button
