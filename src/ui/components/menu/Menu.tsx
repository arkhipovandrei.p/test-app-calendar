import {FC} from "react";
import s from "./styles.module.scss"
import {MenuItemProps} from "./types/MenuProps.ts";
import MenuItem from "./MenuItem.tsx";

const Menu: FC<MenuItemProps> = ({items}) => {
  return <div className={s.menu}>
    {items.map((item) => <MenuItem
      key={`${item.to}-${item.label}`}
      item={item}
    />)}
  </div>
}

export default Menu;
