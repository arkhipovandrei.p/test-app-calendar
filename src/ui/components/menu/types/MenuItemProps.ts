import {MenuItem} from "./MenuItem.ts";

export type MenuItemProps = {
  item: MenuItem
}
