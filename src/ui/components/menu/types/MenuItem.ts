import {type ReactNode} from "react";
import {type LinkProps} from "react-router-dom";

export type MenuItem = {
  isActive?:boolean;
  label:string;
  to?: LinkProps['to'];
  icon?: ReactNode;
}
