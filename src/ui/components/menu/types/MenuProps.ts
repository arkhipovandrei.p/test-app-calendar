import {MenuItem} from "./MenuItem.ts";

export type MenuItemProps = {
  items: Array<MenuItem>
}
