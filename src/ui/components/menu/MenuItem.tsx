import {type FC} from "react";
import s from './styles.module.scss'
import cn from "classnames";
import {MenuItemProps} from "./types/MenuItemProps.ts";
import {NavLink} from "react-router-dom";

const MenuItem: FC<MenuItemProps> = ({item}) => {


  return <NavLink

    className={({isActive}) => {
      return cn(s.menuItem, {
        [s.menuItemActive]: isActive
      });
    }
    }
    to={item.to}>
    {item.icon} {item.label}
  </NavLink>
}

export default MenuItem
