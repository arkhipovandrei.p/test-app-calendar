import {ReactNode, SVGProps} from "react";

type IconSvgProps = SVGProps<SVGSVGElement>;

export type IconProps  = IconSvgProps & {
    size?: number;
    enableBackground?: string;
    fill?: string;
    width?: number;
    height?: number;
    viewBox?: string;
    xmlns?: string;
    className?: string;
    children?: ReactNode
}

export const BaseIcon = (iconProps: IconProps) => {
    const {
       fill = "none",
       size,
       height,
       width,
       xmlns='http://www.w3.org/2000/svg',
       children,
       ...props
    } = iconProps;

    return (
        <svg
            viewBox="0 0 24 24"
            width={size || width || 24}
            height={size || height || 24}
            xmlns={xmlns}
            fill={fill}
            {...props}
        >
            {children}
        </svg>
    );
};
