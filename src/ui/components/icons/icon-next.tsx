import {FC} from "react";
import {BaseIcon, IconProps} from "./base-icon.tsx";

export const IconNext: FC = ({fill = "currentColor", ...props}: IconProps) => {
  return <BaseIcon
    {...props}
  >
    <path
      d="M19 12L5 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14 17L19 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14 7L19 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </BaseIcon>
}
