import {FC} from "react";
import {BaseIcon, IconProps} from "./base-icon.tsx";

export const IconPrev: FC = ({fill = "currentColor", ...props}: IconProps) => {
  return <BaseIcon
    {...props}
  >
    <path
      d="M5 12L19 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 7L5 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10 17L5 12"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </BaseIcon>
}
