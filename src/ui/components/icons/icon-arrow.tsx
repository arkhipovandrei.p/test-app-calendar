import {FC} from "react";
import {BaseIcon, IconProps} from "./base-icon.tsx";

export const IconArrow: FC = ({fill = "currentColor", ...props}: IconProps) => {
  return <BaseIcon
    {...props}
  >
    <path
      d="M8 10L12 14L16 10"
      stroke={fill}
      strokeWidth="1.2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </BaseIcon>

}
