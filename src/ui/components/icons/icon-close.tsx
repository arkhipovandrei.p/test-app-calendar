import {FC} from "react";
import {BaseIcon, IconProps} from "./base-icon.tsx";

export const IconClose: FC = ({fill = "currentColor", ...props}: IconProps) => {
  return <BaseIcon
    {...props}
  >
    <path
      d="M8 8L16 16"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M16 8L8 16"
      stroke={fill}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </BaseIcon>

}
