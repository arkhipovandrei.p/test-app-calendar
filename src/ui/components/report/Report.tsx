const Report = () => {
  return <>
    <div className="report-H1UzCx" data-id="2003:838">
      <div
        className="text_label-QTlIar text_label circerounded-regular-normal-martinique-20px"
        data-id="2003:839"
      >
        Отчеты <br/>
        от учителей
      </div>
      <div className="icon-QTlIar" data-id="2003:840">
        <img
          className="school-learningtest-checkmark-done-Mxj7S3"
          data-id="2003:841"
          src="/projects/6668917bc17770f96de659ac/releases/666a7e3e6c60ea77e60b6e33/img/school--learning-test-checkmark-done.svg"
          anima-src="/projects/6668917bc17770f96de659ac/releases/666a7e3e6c60ea77e60b6e33/img/school--learning-test-checkmark-done.svg"
          alt="School, Learning/test-checkmark-done"
        />
      </div>
    </div>
  </>
}

export default Report;
