import s from './styles.module.scss';
import type {FC} from "react";
import {IconUser} from "../icons/icon-user.tsx";
import User from "../user/User.tsx";
import Button from "../button/Button.tsx";

type ScheduleProps = {
  items: Array<any>
}

const Schedule:FC<ScheduleProps> = (props) => {

  const {
    items
  } = props;

  return <>
    <div className={s.schedule}>
      <div className={s.scheduleHeader}>
        Ближайшие уроки
      </div>
      <div className={s.scheduleList}>
        {items.map(item => <ScheduleItem key={item.key}/>)}
      </div>
      <div className={s.scheduleFooter}>
        <Button className={s.scheduleFooterButton} color={"secondary"} fullWidth >
          Button
        </Button>
      </div>
    </div>
  </>
}

export default Schedule;

const ScheduleItem = () => {
  return <div className={s.scheduleListItem}>
    <time>
      <span className={s.scheduleListItemDay}>1</span> мая
    </time>
    <div className={s.scheduleListItemLessonName}>
      Ментальная Арифметика
    </div>
    <time >
      14:00-14:25
    </time>

    <User
      name={"Белкина Инна"}
      avatar={<IconUser/>}
    />

    <div className={s.scheduleListItemActions}>
      <Button  size={"sm"} color={"secondary"} variant={"bordered"}>Button</Button>
      <Button size={"sm"}>Button</Button>
    </div>

  </div>
}
