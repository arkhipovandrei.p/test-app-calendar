import {type ReactNode} from "react";

export type AdBannerProps = {
  header?: string;
  content?: string;
  image?: ReactNode;
  src?: string;
}
