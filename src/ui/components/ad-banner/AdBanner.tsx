import {type FC,  useMemo} from "react";
import s from './styles.module.scss';
import {AdBannerProps} from "./types/AdBannerProps.ts";


const AdBanner: FC<AdBannerProps> = (props) => {

  const {
    header,
    content,
    image,
    src
  } = props;

  const banner = useMemo(() => {

    if (image) {
      return image
    }

    return <img
      src={src}
      className={s.adBannerImage}
      alt="sale"
    />

  }, [src, image])


  return <>
    <div className={`${s.adBanner} row-span-2`}>
      {banner}
      <h1 className={s.adBannerHeader}>
        {header}
      </h1>
      <p className={s.adBannerContent}>
        {content}
      </p>
    </div>
  </>
}

export default AdBanner;
