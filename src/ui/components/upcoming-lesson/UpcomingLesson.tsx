import s from './styles.module.scss';
import {type FC} from "react";
import Button from "../button/Button.tsx";


const UpcomingLesson = () => {

  return <>
    <div className={`${s.upcomingLesson} row-span-2`}>

      <div className={s.upcomingLessonHeader}>
        Следующее занятие начнется через:
      </div>

      <div className={s.upcomingLessonTimer}>
        <UpcomingLessonTimerCounter
          value={6}
          label={"д"}
        />
        <UpcomingLessonTimerCounter
          value={12}
          label={"ч"}
        />
        <UpcomingLessonTimerCounter
          value={24}
          label={"мин"}
        />
      </div>

      <Button
        size={"lg"}
        color={"default"}
        variant={"dashed"}
      >
        Button
      </Button>

    </div>
  </>
}

export default UpcomingLesson


type UpcomingLessonTimerCounterProps = {
  label: string;
  value: string| number
}
const UpcomingLessonTimerCounter:FC<UpcomingLessonTimerCounterProps> = (props) => {

  const {label, value} = props

  return <div className={s.upcomingLessonTimerCounter}>
    {value}
    <div className={s.upcomingLessonTimerCounterLabel}>
      {label}
    </div>
  </div>
}
