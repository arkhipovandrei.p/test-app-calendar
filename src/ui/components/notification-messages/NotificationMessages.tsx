import {IconMessages} from "../icons/icon-messages.tsx";
import Badge from "../badge/Badge.tsx";
import s from './styles.module.scss'

const NotificationMessages = () => {
  return <Badge value={2} color={"danger"}>
    <div className={s.notificationMessages}>
      <IconMessages/>
    </div>
  </Badge>
}

export default NotificationMessages;
