import s from "./styles.module.scss";
import {type FC} from "react";
import {ReferralProps} from "./types/ReferralProps.ts";
import Button from "../button/Button.tsx";
import {NavLink} from "react-router-dom";


const Referral:FC<ReferralProps> = (props) => {

  const {
    header,
    content,
    to,
    src
  } = props

  return <>
    <div className={s.referral}>
      <h4 className={s.referralHeader}>
        {header}
      </h4>
      <div className={s.referralBody}>
        {content}
      </div>
      {to && <Button
        as={NavLink}
        to={to}
        color={"info"}
        size={"sm"}
        className={s.referralButtonMore}
      >
        Узнать
      </Button>}
      <div className={s.referralGiftIllustration}>
        <img src={src} alt=""/>
      </div>
    </div>
  </>
}

export default Referral;
