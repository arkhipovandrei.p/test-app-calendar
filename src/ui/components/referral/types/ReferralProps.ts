import {type LinkProps} from "react-router-dom";
import {type ImgHTMLAttributes} from "react";

export type ReferralProps = {
  header?: string;
  content?: string;
  to?: LinkProps["to"]
  src: ImgHTMLAttributes<HTMLImageElement>['src']
}
