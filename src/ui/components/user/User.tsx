import {FC, ReactNode} from "react";
import s from './styles.module.scss'

type UserProps = {
  avatar?: ReactNode;
  name?: string;
  description?: string | null;
}
const User: FC<UserProps> = (props) => {

  const {
    avatar,
    name, description
  } = props

  return <div className={s.user}>
    {avatar && <div className={s.userAvatar}>
      {avatar}
    </div>}
    <div className={s.userProfile}>
      <div className={s.userName}>
        {name}
      </div>
      {description && <div className={s.userDescription}>
        {description}
      </div>}
    </div>

  </div>
}

export default User;
