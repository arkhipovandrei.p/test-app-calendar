import Menu from "../menu/Menu.tsx";
import {IconHome} from "../icons/icon-home.tsx";
import {IconCalendar} from "../icons/icon-calendar.tsx";
import {IconMoneyWallet} from "../icons/icon-money-wallet.tsx";
import s from "./styles.module.scss"
import Referral from "../referral/Referral.tsx";
import {ReferralProps} from "../referral/types/ReferralProps.ts";
import {MenuItem} from "../menu/types/MenuItem.ts";

const menuItems:MenuItem[] = [
  {label: "Главная", to: "/", icon: <IconHome/>, isActive: true},
  {label: "Расписание", to: "/calendar", icon: <IconCalendar/>},
  {label: "Оплата", to: "/payment", icon: <IconMoneyWallet/>},
];

const referral:ReferralProps = {
  header: "Учитесь бесплатно",
  content: "Приводите друзей с детьми заниматься в Sirius Future и получайте подарки!",
  to: "#",
  src: "/img/gift-illustration.svg"
}

const Sidebar = () => {
  return <div className={s.sidebar}>
    <div className={s.sidebarLogo}>
      <img
        src="/img/logo-2.svg"
        alt="logo"
      />
    </div>
    <Menu items={menuItems}/>
    <Referral
      {...referral}
    />
  </div>
}
export default Sidebar;
