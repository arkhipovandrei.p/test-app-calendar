import {IconExit} from "../icons/icon-exit.tsx";
import {logout} from "../../../features/auth/app/stores/authSlice.ts";
import {useDispatch} from "react-redux";

const SignOut = () => {
  const dispatch = useDispatch()

  const handleSignOut = () => {
    dispatch(logout())
  }

  return <>
    <a href="#" className={"signout"} onClick={handleSignOut}>
      Выход
      <IconExit/>
    </a>
  </>
}

export default SignOut;
