import {useMemo} from "react";
import {useGetUsersQuery} from "@/features/user/app/stores/usersSlice.ts";
import User from "@/ui/components/user/User.tsx";
import Avatar from "@/ui/components/avatar/Avatar.tsx";
import {useGetMeQuery} from "@/features/auth/app/api/authApi.ts";

export const useNavbar = () => {
  const {data: currentUser} = useGetMeQuery('userDetails');
  const {data} = useGetUsersQuery(2);

  const availableSessions = useMemo(() => {

    if (!data?.users) return null;
    return data?.users?.map((user) => {
      const isActive = user?.id === currentUser?.id
      return {
        label: <User
          avatar={<Avatar src={user.image}/>}
          name={user.username}
          description={isActive ? "Это вы" : null}
        />,
        key: user.id,
        isActive
      }
    })
  }, [data?.users, currentUser?.id])

  return {
    availableSessions,
    currentUser,
  }
}
