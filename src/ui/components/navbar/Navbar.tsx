import NotificationMessages from "../notification-messages/NotificationMessages.tsx";
import Dropdown from "../dropdown/Dropdown.tsx";
import {useNavbar} from "./useCases/useNavbar.tsx";
import SignOut from "./SignOut.tsx";
import Avatar from "../avatar/Avatar.tsx";
import {FC, ReactNode} from "react";


const Navbar = () => {

  const {currentUser, availableSessions} = useNavbar();

  return <div className="layout__header">
    <div>
      {currentUser && <Notify>
        Добро пожаловать, <span className="text--dark-violet">
      {currentUser?.firstName}
    </span>!
      </Notify>}
    </div>
    <div className={"layout__header__right "}>
      <NotificationMessages/>
      <Dropdown
        showArrow={true}
        contentProps={{
          header: "Смена пользователя",
          footer: <SignOut/>,
          items: availableSessions
        }}
      >
        <Avatar src={currentUser?.image} alt={currentUser?.username}/>
      </Dropdown>
    </div>
  </div>
}
export default Navbar;


//todo:: это не точно, нужно еще раз подумать но выглядит как нотификация
const Notify: FC<{ children?: ReactNode }> = (props) => {
  const {children} = props
  return <span className="layout__header__title">
    {children}
  </span>
}
