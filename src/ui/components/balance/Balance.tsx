import s from './styles.module.scss';
import {type FC} from "react";
import Button from "../button/Button.tsx";


const Balance: FC<{ items: Array<any> }> = ({items}) => {
  return <>
    <div className={s.balance}>
      <div className={s.balanceHeader}>
        Баланс занятий
      </div>
      <ul className={s.balanceList}>
        {items.map((balance, i) => {
          return <li key={balance.label+i} className={s.balanceItem}>
            <span className={s.balanceLabel}>
              {balance.label}
            </span>
            <span className={s.balanceValue}>
            {balance.value}
          </span>
          </li>
        })}
      </ul>

      <div className={s.balanceFooter}  >
        <Button color={"secondary"} fullWidth>
          Button
        </Button>
      </div>
    </div>

  </>
}
export default Balance;
