import type {FC, ReactNode} from "react";
import s from './styles.module.scss'
import cn from "classnames";


type BadgeProps = {
  children: ReactNode;
  value?: null | string | number;
  color?: 'danger'
}

const Badge: FC<BadgeProps> = (props) => {

  const {
    children,
    color ,
    value
  } = props

  return <div
    className={cn(s.badge, {
      [s[`badge--${color}`]]: color
    })
    }>
    <div className={s.badgeValue}>
      {value}
    </div>
    {children}
  </div>
}

export default Badge;
