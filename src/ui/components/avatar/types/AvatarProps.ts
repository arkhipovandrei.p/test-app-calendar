import {ImgHTMLAttributes} from "react";

export type AvatarProps = ImgHTMLAttributes<HTMLImageElement>
