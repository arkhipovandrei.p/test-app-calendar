import {FC} from "react";
import s from './styles.module.scss'
import {AvatarProps} from "./types/AvatarProps.ts";

const Avatar: FC<AvatarProps> = ({src, alt}) => {
  return <div className={s.avatar}>
    <img src={src} alt={alt}/>
  </div>
}

export default Avatar;
