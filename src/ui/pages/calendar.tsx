import Calendar from "../components/calendar/CalendarProps.tsx";
import {Dayjs} from "dayjs";
import {FC, useState} from "react";
import {IconArrow} from "../components/icons/icon-arrow.tsx";
import Button from "../components/button/Button.tsx";

type Event = {
  id: number;
  name: string
}

const CalendarPage:FC = () => {

  const [events, setEvents] = useState<Array<Event>>([])

  const handleChange = async (date:Dayjs) => {

  }

  return <div className={"page page--calendar"}>

    <div className={"page--calendar__header"}>
      <div className={"select"} >
        <div >Выбрать предмет</div>
        <IconArrow/>
      </div>
      <Button size={"lg"} color={"secondary"}>
        Изменить расписание
      </Button>
    </div>
    <Calendar events={events} onChange={handleChange}/>
  </div>
}

export default CalendarPage;
