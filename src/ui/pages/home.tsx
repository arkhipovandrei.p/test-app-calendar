import AdBanner from "../components/ad-banner/AdBanner.tsx";
import UpcomingLesson from "../components/upcoming-lesson/UpcomingLesson.tsx";
import Balance from "../components/balance/Balance.tsx";
import Schedule from "../components/schedule/Schedule.tsx";
import InfoBox from "../components/info-box/InfoBox.tsx";
import {IconLearningBook} from "../components/icons/icon-learning-book.tsx";
import {IconTestCheckmarkDone} from "../components/icons/icon-test-checkmark-done.tsx";

const userBalance = [
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
]
const schedule = [
  {label: "Ментальная Арифметика", from: '14:00', to: "14:25", date: "1 may", teacher: {name: "Животновская Оксана"}},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
  {label: "Ментальная Арифметика", value: 32,},
  {label: "Программирование", value: 0,},
  {label: "Скорочтение", value: 0,},
  {label: "Арифметика", value: 4,},
]
const HomePage = () => {
  return <div className={"page page--home"}>
    <AdBanner
      src="/img/sf-2-1@2x.png"
      header={"До 31 декабря любой курс со скидкой 20%"}
      content={"До конца года у вас есть уникальная возможность воспользоваться нашей новогодней скидкой 20% на любой курс!"}
    />
    <UpcomingLesson/>

    <div className={"page--home__info-boxes"}>

      <InfoBox color={"blue"} header={"Домашние задания"} icon={<IconLearningBook/>} />
      <InfoBox color={"violet"} header={"Отчеты от учителей"} icon={<IconTestCheckmarkDone/>}/>

    </div>

    <Balance items={userBalance}/>
    <Schedule items={schedule}/>
  </div>
}

export default HomePage;





