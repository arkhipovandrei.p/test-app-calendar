import {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {useForm} from "react-hook-form";
import {userLogin} from "@/features/auth/app/stores/authActions.ts";
import LoginForm from "@/features/auth/ui/components/login-form/login-form.tsx";
import {AuthState} from "@/features/auth/app/stores/authSlice.ts";

const Login: FC = () => {

  const {userToken, loading, error} = useSelector((state: { auth: AuthState }) => state.auth)
  const dispatch = useDispatch()
  const form = useForm<{ username: string; password: string; remember?: boolean }>({
    defaultValues: {
      username: "",
      password: "",
      remember: false,
    }
  })
  const navigate = useNavigate()

  useEffect(() => {
    if (userToken) {
      navigate('/')
    }
  }, [navigate, userToken])

  const submitForm = (data) => {
    dispatch(userLogin(data))
  }

  return <div className={"page page--auth"}>
    <LoginForm loading={loading} form={form} onSubmit={form.handleSubmit(submitForm)} error={error}/>
    <div className={"change-lang"}>
      <button className={"btn btn-active"}>RU</button>
      <button className={"btn"}>EN</button>
    </div>
  </div>
}

export default Login

