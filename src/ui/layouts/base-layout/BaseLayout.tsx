import {FC, ReactNode} from "react";
import Sidebar from "../../components/sidebar/Sidebar.tsx";
import Navbar from "../../components/navbar/Navbar.tsx";

const BaseLayout: FC<{ children: ReactNode }> = ({children}) => {

  return <div className={"layout"}>
    <Sidebar/>
    <main className={"layout__content"}>
      <Navbar/>
      {children}
    </main>
  </div>
}
export default BaseLayout;
