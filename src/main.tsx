import ReactDOM from 'react-dom/client'
import { StrictMode } from 'react';

import {
  BrowserRouter,
} from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux"

import './ui/styles/reset.min.css';
/*
  в макете используется платный рифт поэтому он был взят с сайта компании разместившей вакансию
*/
import './ui/styles/fonts.css';
import './ui/styles/index.css';

import store from "./app/stores";
import App from "./App.tsx";

ReactDOM.createRoot(document.getElementById('root')!).render(
  <StrictMode>
    <ReduxProvider store={store}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </ReduxProvider>
  </StrictMode>,
)
