import {FC, FormEvent, useState} from "react";
import {IconEye} from "@/ui/components/icons/icon-eye.tsx";

const LoginForm: FC<{
  onSubmit(event: FormEvent<HTMLFormElement>): Promise<void>;
  form:any;
  loading:boolean;
  error?:Error | null
}> = (props) => {

  const {onSubmit, form, loading, error} = props;

  const [inputType, setInputType] = useState("password");

  const toggle = () => setInputType(prevState => {
    if (prevState === 'text') return 'password'
    return 'text'
  })

  return <div className={"auth"}>
    <img className={"auth__logo"} alt="logo" src={"/logo.svg"}/>
    <h4 className={"auth__header"}>
      Вход в Sirius Future
    </h4>
    <form className={"auth__form"} onSubmit={onSubmit}>
      <div className="form-item">
        <input {...form.register('username')} type={"text"} placeholder={"E-mail"} required disabled={loading}/>
        <p className={"form-item__description"}>
          demo username: 'emilys'
        </p>
      </div>
      <div className="form-item">
        <div className={"form-item"}>
          <input
            {...form.register('password')} placeholder={"Пароль"}
            className={"input--password"}
            type={inputType}
            required
            disabled={loading}
          />
          <span className={"toggle-input-type"} onClick={toggle}>
            <IconEye/>
          </span>
        </div>
        <p className={"form-item__description"}>
          demo password: 'emilyspass'
        </p>
      </div>
      <div className="form-item">
        <label className={"remember-me"}>
          <input  {...form.register('remember')} type={"checkbox"}/> Запомнить меня
        </label>
      </div>
      <button type={"submit"} disabled={loading}>Войти</button>

      <p className={"text-danger"}>{error}</p>
    </form>
    <div className="auth__forgot-password">
      <a href="#">Я забыл пароль</a>
      <a href="#">Войти как тренер</a>
    </div>
    <div className={"auth__footer"}>
      Нет аккаунта?
      <a href="#">Зарегистрироваться</a>
    </div>
  </div>
}

export default LoginForm;
