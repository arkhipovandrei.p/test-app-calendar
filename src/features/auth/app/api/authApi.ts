import {createApi} from '@reduxjs/toolkit/query/react'
import {baseQuery} from "@/app/stores/baseQuery.ts";


export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery,
  endpoints: (build) => ({
    getMe: build.query({
      query: () => ({
        url: '/auth/me',
        method: 'GET',
      }),
    }),
  }),
})

export const { useGetMeQuery } = authApi
