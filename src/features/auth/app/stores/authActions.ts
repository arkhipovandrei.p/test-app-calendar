import {createAsyncThunk} from '@reduxjs/toolkit'

const baseUrl = import.meta.env.VITE_API_URL

export const userLogin = createAsyncThunk(
  'auth/login',
  async ({remember, ...credential}: { username: string; password: string; remember?: boolean }, {rejectWithValue}) => {
    try {
      const response = await fetch(`${baseUrl}/auth/login`, {
        method: 'POST',
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          ...credential,
          expiresInMins: 60 * (remember ? 1 : 24)
        })
      })
      const data = await response.json();

      if(!data.token) {
        return rejectWithValue(data.message ?? " Invalid credentials")
      }

      localStorage.setItem('userToken', data.token)
      return data;

    } catch (error) {
      return error;
    }
  }
)

