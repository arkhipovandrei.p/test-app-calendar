import {createSlice} from '@reduxjs/toolkit'
import {userLogin} from './authActions.ts'
import {User} from "@/features/user/app/types/User.ts";

const userToken = localStorage.getItem('userToken')
  ? localStorage.getItem('userToken')
  : null

export type AuthState = {
  loading: boolean;
  userInfo: null | User;
  userToken: null | string,
  error: null | string,
  success: boolean,
}

const initialState = {
  loading: false,
  userInfo: null,
  userToken,
  error: null,
  success: false,
} satisfies AuthState as AuthState;

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: (state) => {
      localStorage.removeItem('userToken')
      state.loading = false
      state.userInfo = null
      state.userToken = null
      state.error = null
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(userLogin.fulfilled, (state, {payload}) => {
        state.loading = false
        state.userInfo = payload
        state.userToken = payload?.token
      })
      .addCase(userLogin.pending, (state) => {
        state.loading = true
        state.error = null
      })
      .addCase(userLogin.rejected, (state, {payload}) => {
        state.loading = false
        state.error = String(payload)
      })
  },
})

export const {logout} = authSlice.actions

export default authSlice.reducer
