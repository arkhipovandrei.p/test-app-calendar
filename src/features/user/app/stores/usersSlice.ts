import {createApi} from "@reduxjs/toolkit/query/react"
import {UsersResponse} from "../types/UsersResponse.ts";
import {baseQuery} from "@/app/stores/baseQuery.ts";


export const usersSlice = createApi({
  baseQuery,
  reducerPath: "users",
  tagTypes: ["Users"],
  endpoints: build => ({
    getUsers: build.query<UsersResponse, number>({
      query: (limit = 10) => `/users?limit=${limit}`,
    }),
    getBalance: build.query<UsersResponse, number>({
      query: (limit = 10) => `/products?limit=${limit}`,
    }),
  }),
})

export const { useGetUsersQuery, useGetBalanceQuery } = usersSlice

export default usersSlice.reducer
