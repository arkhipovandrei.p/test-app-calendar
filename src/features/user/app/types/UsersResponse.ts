import {User} from "./User.ts";

export type UsersResponse = {
  users: User[];
  total: number;
  skip: number;
  limit: number;
}
