export type User = {
  id: ID;
  firstName: string;
  lastName: string;
  maidenName?: string;
  age: number;
  gender: string;
  email: Email;
  phone: Phone;
  username: string;
  password?: string;
  birthDate?: Date | string;
  image?: string;
}
