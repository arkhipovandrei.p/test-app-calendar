import {ConfigType, Dayjs, ManipulateType} from "dayjs";
import dayjs from "dayjs";

export function dayjsRange (from: ConfigType, to:ConfigType, step :number = 1, unit: ManipulateType = 'day', format?: string):Array<Dayjs| string> {
  const list = []
  let current = dayjs(from)
  while (current.isBefore(to)) {
    list.push(format ? current.format(format) : current)
    current = current.add(step , unit)
  }
  return list
}

