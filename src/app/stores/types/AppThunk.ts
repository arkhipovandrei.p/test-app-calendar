import type {Action, ThunkAction} from "@reduxjs/toolkit";
import {RootState} from "./RootState.ts";

export type AppThunk<ThunkReturnType = void> = ThunkAction<
  ThunkReturnType,
  RootState,
  unknown,
  Action
>
