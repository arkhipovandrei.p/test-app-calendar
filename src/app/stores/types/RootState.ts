import {rootReducer} from "../rootReducer.ts";

export type RootState = ReturnType<typeof rootReducer>
