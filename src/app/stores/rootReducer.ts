import {combineSlices} from "@reduxjs/toolkit";
import {usersSlice} from "../../features/user/app/stores/usersSlice.ts";

export const rootReducer = combineSlices( usersSlice)
