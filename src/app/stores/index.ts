import {configureStore} from '@reduxjs/toolkit'
import authReducer from '@/features/auth/app/stores/authSlice.ts'
import userReducer, {usersSlice} from "@/features/user/app/stores/usersSlice.ts";
import {authApi} from "@/features/auth/app/api/authApi.ts";

const store = configureStore({
  reducer: {
    auth: authReducer,
    users: userReducer,
    [authApi.reducerPath]: authApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(authApi.middleware).concat(usersSlice.middleware),
})

export default store
