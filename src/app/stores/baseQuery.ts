import {fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {AuthState} from "@/features/auth/app/stores/authSlice.ts";

const baseUrl = import.meta.env.VITE_API_URL

export const baseQuery = fetchBaseQuery({
  baseUrl,
  prepareHeaders: (headers, {getState}) => {
    const { auth } = getState() as { auth: AuthState };
    const token = auth.userToken;
    if (token) {
      headers.set('authorization', `Bearer ${token}`)
      return headers
    }
  },
})
