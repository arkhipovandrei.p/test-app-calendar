type Email = string;
type Phone = string;
type ID = number;
type DateTimeString = string;
type DeletedAt = string;
type Price = string | number;
