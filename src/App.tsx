import '@/ui/styles/App.css'
import {Navigate, Outlet, Route, Routes,} from "react-router-dom";
import RequireAuth from "@/ui/components/routing/RequireAuth.tsx";
import Login from "@/ui/pages/login.tsx";
import Home from "@/ui/pages/home.tsx";
import Calendar from "@/ui/pages/calendar.tsx";
import Payment from "@/ui/pages/payment.tsx";

export default function App() {
  return (
    <Routes>
      <Route element={<Outlet/>}>
        <Route path="/login" element={<Login/>}/>
        <Route path={"/"} element={<RequireAuth/>}>
          <Route path={""} element={<Home/>}/>
          <Route path="/calendar" element={<Calendar/>}/>
          <Route path="/payment" element={<Payment/>}/>
        </Route>
      </Route>
      <Route path='*' element={<Navigate to='/' replace/>}/>
    </Routes>
  );
}
